﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

    string minutes;
    string seconds;
    float timer;
    float startVal;
    bool survivalPeriod;

	// Use this for initialization
	void Start ()
    {
        startVal = 10.0f;
        timer = startVal;
        survivalPeriod = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer -= Time.deltaTime;

        if (timer < 0)
        {
            if (survivalPeriod && startVal > 0.5)
            {
                startVal -= 0.5f;

                survivalPeriod = false;
            }

            else
            {
                survivalPeriod = true;
            }

            timer = startVal;
        }

        minutes = Mathf.Floor(timer / 60).ToString("00");
        seconds = (timer % 60).ToString("00");
    }

    void OnGUI()
    {
        if (survivalPeriod)
        {
            GUI.Label(new Rect(10, 10, 250, 100), "Survive: " + minutes + ":" + seconds);
        }

        else
        {
            GUI.Label(new Rect(10, 10, 250, 100), "Prepare: " + minutes + ":" + seconds);
        }
    }
}
