﻿using UnityEngine;
using System.Collections;
using System;

public class Fight : MonoBehaviour
{
    private bool firstRun = true;
    private double direction = 0.0d;
    private double angleCurrentlyFacing = 0.0d;
    private double angleToRotate = 0.0d;
    private double distance = 4.0d;
    private double speed = 0.02d;
    private double xdist = 0.0d;
    private double zdist = 0.0d;
    private float lastChanged = 0.0f;
    private float travelTime = 0.0f;
    private Vector3 destination;
    private GameObject destinationTransformObject;

    void Start()
    {
        destinationTransformObject = new GameObject("Target");
    }

    void Update()
    {
        if (!firstRun)
        {
            firstRun = true;
        }

        if (Time.time >= lastChanged + travelTime)
        {
            directionChange(randAngleGenerator(0, 360));
            timeChange();

            lastChanged = Time.time;
        }

        transform.position = Vector3.Lerp(
            transform.position,
            destination,
            (float)speed);
            
        Debug.DrawLine(this.transform.position, destination, Color.red);
        Debug.DrawLine(this.transform.position, transform.position + transform.forward, Color.blue);

        Ray colisionDetector = new Ray(this.transform.position, destination - this.transform.position);

        if (Physics.SphereCast(colisionDetector, 0.5f, 4.0f))
        {
            directionChange(randAngleGenerator(0, 360));
        }   
    }

    double randAngleGenerator(int a_iMin, int a_iMax)
    {
        double angle = new double();

        angle = UnityEngine.Random.Range(a_iMin, a_iMax);

        return angle;
    }

    void directionChange(double a_iAngle)
    {
        xdist = System.Math.Sin(a_iAngle) * distance;
        zdist = System.Math.Cos(a_iAngle) * distance;

        destination = new Vector3(
            (float)xdist,
            transform.position.y,
            (float)zdist);

        destinationTransformObject.transform.position = destination;

        transform.LookAt(destinationTransformObject.transform);
    }

    void timeChange()
    {
        travelTime = UnityEngine.Random.Range(0.0f, 10.0f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            directionChange(findAngleToPlayer());
            timeChange();

            lastChanged = Time.time;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            directionChange(randAngleGenerator(0, 360));
            timeChange();

            lastChanged = Time.time;
        }
    }
    double findAngleToPlayer()
    {
        var player = GameObject.FindWithTag("Player");

        Vector3 vectToPlayer = new Vector3();
        vectToPlayer = this.transform.position - player.transform.position;

        double angle = new double();

        angle = System.Math.Atan(vectToPlayer.x / vectToPlayer.z);

        angle = (angle * 180) / Math.PI;

        return angle;
    }
}
